/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package container;

import static container.NewDialogTB.tableModel;
import java.awt.Component;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 *
 * @author nwni
 */
public class IterateTable {

    final static IterateTable objIterateT = new IterateTable();
    JComponent component1;
    Component comp;
    String everything;
    static ArrayList al = new ArrayList();

    //Itera toda la tabla y busca por espacios en blanco, imprimiendo en un Label cantidad de celdas en blanco para cada tabla que se seleccione
    public void iterateTable() throws IOException {
        int rows = tableModel.getRowCount();
        int columns = tableModel.getColumnCount();
        int x = 0, vacios = 0;
        String columnName;

        String encontrado, id;
        al.clear();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                //Se extrae el Objeto de la celda i,j y se convierte a String
                encontrado = (String) tableModel.getValueAt(i, j);
                //Se le agrega la condicion (encontrado==null) para los campos null de la tabla expediente
                id = (String) tableModel.getValueAt(i, 0);
                if ((encontrado == null) || (encontrado.matches(" "))) {
                    //System.out.println("Encontrado -> "+id);
                    al.add(id);//Array list con los id de las filas que tienen elementos vacios
                    vacios++;
                }
                
                columnName = tableModel.getColumnName(j);
                
                switch(columnName){
                    case "Telefono": //En la columna Telefono, si el campo no cumple con la cantidad de digitos, se elimina
                        if(tableModel.getValueAt(i, j).toString().length() > 12 || (tableModel.getValueAt(i, j).toString().length() > 1 && tableModel.getValueAt(i, j).toString().length() < 12)){
                            //System.out.println(tableModel.getValueAt(i, j));
                            al.add(id); //Se agrega a la lista con los datos a eliminar
                        }
                        break;
                    case "Cirugia": //Si el ID de la cirugia no esta en el rango permitido, se elimina el registro
                        //Se obtiene el valor de la celda, se convierte a String y se elimina el espacio al principio de la linea
                        //Se convierte a INT y se compara con el rango.
                        if(Integer.parseInt(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) > 45){
                            //System.out.println("Cirugia-> "+tableModel.getValueAt(i, j));
                            al.add(id); //Se agrega a la lista con los datos a eliminar
                        }
                        break;
                    case "Area": //El campo tiene que ser uno de los siguientes(para las tablas 'Empleado' y 'Cuarto'), de lo contrario, se elimina el campo
                        if((("Doctor".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) || 
                                "Enfermero".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), ""))) || 
                                "Laboratorista".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), ""))) ||
                                "Recepcionista".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) ||
                                "Urgencias".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) ||
                                "Terapia".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) ||
                                "Dialisis".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) ||
                                "Recuperacion".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) ||
                                "Pediatria".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) ||
                                "Preventiva".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), "")) ||
                                "Hematologia".equals(tableModel.getValueAt(i, j).toString().replaceAll(("^\\s+"), ""))){
                        } else {
                            //System.out.println(tableModel.getValueAt(i, j));
                            al.add(id); //Se agrega a la lista con los datos a eliminar
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        x = 0;
        //mainF.jLabel4.setText("Se encontraron: " + vacios + " campos vacios en la tabla " + FileActions.tablaF);
    }
}
