/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package container;

import static container.NewDialogTB.tableModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author nwni
 */
public class NewDialogTBEmptyCells extends NewDialogTB {
//Se crea un nuevo frame SOLO CON UNA TABLA!

    static final NewDialogTBEmptyCells objNewDialog1 = new NewDialogTBEmptyCells();

    //static String[] tableDoctor = {"No Identificador","Especialidad"};
    static DefaultTableModel tableModel1;
    private final Border outside = new MatteBorder(1, 0, 1, 0, Color.RED);
    private final Border inside = new EmptyBorder(0, 1, 0, 1);
    private final Border highlight = new CompoundBorder(outside, inside);

    int vacios = 0;

    //tableModel  = new DefaultTableModel(0, 2);
    @Override
    public void newTable() {

        Dimension paneSize = new Dimension(1000, 550);

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JDialog dialog = new JDialog();
                dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                JPanel panel = new JPanel();
                JTabbedPane tabbedPane1 = new JTabbedPane();
                panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
                panel.setPreferredSize(paneSize);
                panel.setOpaque(true);
                JTable table1 = new JTable(tableModel) {

                    @Override
                    public Component prepareRenderer(TableCellRenderer renderer, int rowIndex, int columnIndex) {

                        Component component = super.prepareRenderer(renderer, rowIndex, columnIndex);

//                        for (int i = 0; i < tableModel.getColumnCount(); i++) {
                        //System.out.println(getValueAt(rowIndex, columnIndex));
                        Object value = getValueAt(rowIndex, columnIndex);
                        String valueS = value.toString().replaceAll(("^\\s+"), "");
                        switch (getColumnName(columnIndex)) {
                            case "Telefono":
                                if ((value.toString().length() > 12 || (value.toString().length() > 1 && value.toString().length() < 12))) {
                                    component.setBackground(Color.cyan);
                                    //component.setBackground(new java.awt.Color(255, 255, 255));   //SI 
                                } else {
//                                    component.setBackground(new java.awt.Color(255, 255, 255));   //SI 
                                if (((getValueAt(rowIndex, columnIndex) == null) || (getValueAt(rowIndex, columnIndex).toString().matches(" ")))) {

                                    //component.setBackground(new java.awt.Color(255, 255, 255));   //SI  
                                    component.setBackground(new java.awt.Color(255, 196, 75));   //SI 

                                } else {
                                    component.setBackground(new java.awt.Color(255, 255, 255));   //SI 
                                }
                                }
                                break;
                            case "Cirugia": //Si el ID de la cirugia no esta en el rango permitido, se elimina el registro
                                //Se obtiene el valor de la celda, se convierte a String y se elimina el espacio al principio de la linea
                                //Se convierte a INT y se compara con el rango.
                                if (Integer.parseInt(value.toString().replaceAll(("^\\s+"), "")) > 45) {
                                    //System.out.println("Cirugia-> "+tableModel.getValueAt(i, j));
                                    component.setBackground(Color.GREEN);
                                } else {
//                                    component.setBackground(new java.awt.Color(255, 255, 255));   //SI 
                                if (((getValueAt(rowIndex, columnIndex) == null) || (getValueAt(rowIndex, columnIndex).toString().matches(" ")))) {

                                    //component.setBackground(new java.awt.Color(255, 255, 255));   //SI  
                                    component.setBackground(new java.awt.Color(255, 196, 75));   //SI 

                                } else {
                                    component.setBackground(new java.awt.Color(255, 255, 255));   //SI 
                                }
                                }
                                break;
                            case "Area": //El campo tiene que ser uno de los siguientes(para las tablas 'Empleado' y 'Cuarto'). Si no concuerda, se pinta de amarillo
                                if (("Doctor".equals(valueS)
                                        || "Enfermero".equals(valueS)
                                        || "Laboratorista".equals(valueS)
                                        || "Recepcionista".equals(valueS)
                                        || "Urgencias".equals(valueS)
                                        || "Terapia".equals(valueS)
                                        || "Dialisis".equals(valueS)
                                        || "Recuperacion".equals(valueS)
                                        || "Pediatria".equals(valueS)
                                        || "Preventiva".equals(valueS)
                                        || "Hematologia".equals(valueS))) {
                                    
                                    //System.out.println(valueS);
//                                    component.setBackground(new java.awt.Color(255, 255, 255));   //SI 
                                if (((getValueAt(rowIndex, columnIndex) == null) || (getValueAt(rowIndex, columnIndex).toString().matches(" ")))) {

                                    //component.setBackground(new java.awt.Color(255, 255, 255));   //SI  
                                    component.setBackground(new java.awt.Color(255, 196, 75));   //SI 

                                } else {
                                    component.setBackground(new java.awt.Color(255, 255, 255));   //SI 
                                }
                                } else {
                                    //System.out.println(tableModel.getValueAt(i, j));
                                    component.setBackground(Color.YELLOW);
                                }
                                break;
                            default:
                                if (((getValueAt(rowIndex, columnIndex) == null) || (getValueAt(rowIndex, columnIndex).toString().matches(" ")))) {

                                    //component.setBackground(new java.awt.Color(255, 255, 255));   //SI  
                                    component.setBackground(new java.awt.Color(255, 196, 75));   //SI 

                                } else {
                                    component.setBackground(new java.awt.Color(255, 255, 255));   //SI 
                                }
                                break;
                        }
//                        }

                        setSelectionBackground(new java.awt.Color(0, 0, 0)); //Color de FONDO SI
                        setSelectionForeground(new java.awt.Color(0, 0, 139).brighter()); //Color de la letra cuando se selecciona SI!

                        return component;
                    }

                    @Override //Para bloquear la edicion de la tabla
                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return false;
                    }
                };
                table1.changeSelection(0, 0, false, false);
                resizeColumnWidth(table1);
                JScrollPane scroller = new JScrollPane(table1);
                scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
                scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
                JPanel inputpanel = new JPanel();
                inputpanel.setLayout(new FlowLayout());
                JButton buttonDeleteEmpty = new JButton("Borrar Vacios");
                buttonDeleteEmpty.addActionListener(new java.awt.event.ActionListener() {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        try {
                            buttonActionPerformedDelete(evt);
                        } catch (IOException ex) {
                            Logger.getLogger(NewDialogTB.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                JButton buttonSaveChanges = new JButton("Guardar");
                buttonSaveChanges.addActionListener(new java.awt.event.ActionListener() {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        try {
                            buttonActionPerformedSave(evt);
                        } catch (IOException ex) {
                            Logger.getLogger(NewDialogTB.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                panel.add(scroller);
                inputpanel.add(buttonDeleteEmpty);
                inputpanel.add(buttonSaveChanges);
                panel.add(inputpanel);
                dialog.setModal(true);
                dialog.getContentPane().add(BorderLayout.CENTER, panel);
                tabbedPane1.add("Ventana", panel); //Agrego el panel a la ventana
                dialog.add(tabbedPane1); //Agrego la ventana al jdialog
                dialog.pack();
                dialog.setLocationByPlatform(true);
                dialog.setVisible(true);
                dialog.setResizable(true);

            }
        });
    }

    @Override
    public void resizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 15; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width + 1, width);
            }
            if (width > 300) {
                width = 300;
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }

    private void buttonActionPerformedDelete(java.awt.event.ActionEvent evt) throws IOException {
        FileActions.objLoadF.removeRow();
        FileActions.objLoadF.capitalizarDatos();
        JOptionPane.showMessageDialog(null, "Guarda tus cambios antes de salir!!");
    }

    private void buttonActionPerformedSave(java.awt.event.ActionEvent evt) throws IOException {
        //Se vuelve a cargar el modelo iterando la tabla
        IterateTable.objIterateT.iterateTable();
        JOptionPane.showMessageDialog(null, "Cambios guardados con �xito!");
    }
}
